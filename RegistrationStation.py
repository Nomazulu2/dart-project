"""
Registration Station project
"""


def read_file(file_name):

    """
    Read and return contents of text file
    """
    with open('bootcampers.txt')as f:
        lines = f.readlines
    return f.readline() 
        
    

def input_user_name():

    """
    Takes username as input
    """
    username = input("Select Username: " )
    return username


def correct_or_incorrect():

    """
    Prompt to ask if details are correct or not
    @return correct or incorrect
    """
    if answer.lower()=="y":
        return "Correct"
    else:
        return "Incorrect"
        
            


def correct_details():

    """
    Prompt to correct and write user details to text file, which includes:
    * Username
    * Date
    * Location
    * Experience
    """
    Username = input("Select Usename: ")
    Date = input( "Enter Date: ")
    Location = input("Enter your location: ")
    Experience =input("Enter your Experience: ")

def get_file_contents():

    """Return desired text file"""
    return "bootcampers.txt"

def find_username(file_name):

    """
    Main functiontion for running Registration Station, which inlcude:
       * get username input from user
       * check if username exists and print corresponding details
    @return corresponding information for username
       """
    username=input_user_name()
    f = open(file_name, "r")
    idata = f.readlines()
    for user in idata :
        if username in user:
         print(user)
        return user

if __name__ == "__main__":
    registrations_file = get_file_contents()
    information = find_username(registrations_file)
    while True:

        answer = correct_or_incorrect()
        if answer == "correct":
            print(information)
            break
        else:
            correct_details()
